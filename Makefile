##
# fzf-gitignore-io
#
# @file
# @version 0.1

include config.mk

SRC = gitignore-io

all:
	@echo "Default task does nothing."

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f gitignore-io-fzf ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/gitignore-io-fzf

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/gitignore-io-fzf
